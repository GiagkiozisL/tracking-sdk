package com.example.octopusdemo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.octopus.managersdk.Utils;
import com.octopus.managersdk.sync_adapter.SyncAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String token = "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJzZXR0aW5ncyI6eyJwYWNrYWdlIjoiY29tLmFrYXpvby5yYWRpbyIsImF1dGgiOiJodHRwczovL29jdG9wdXMuYWxlbWFndS5jb20vYXV0aC9zZGsvbG9naW4iLCJleHAiOjYwMCwidGRvbWFpbiI6Imh0dHBzOi8vb2N0b3B1cy5hbGVtYWd1LmNvbS90cmsvIiwiZnJxIjo2MDAsInNka3MiOlsicnRiIiwiZHQiXSwic2RrX2RhdGEiOlt7InJ0YiI6eyJsaWJwYXRoIjoiaHR0cHM6Ly9zdG9yYWdlLmdvb2dsZWFwaXMuY29tL29jdG9wdXMtc2RrL3J0YnY0LmphciIsIm1ldGhvZCI6ImV4ZWN1dGVNZXRob2RDIiwiY2xhc3MiOiJjb20ub2N0b3B1cy5ydGIuQ0NsYXNzIiwicGFyYW1ldGVycyI6eyJmZXRjaCI6Imh0dHBzOi8vb2N0b3B1cy1ydGIuYWxlbWFndS5jb20vYWN0aW9ucy9ydGIvdmlldyJ9LCJzdG9wIjp7Im1ldGhvZCI6ImV4ZWN1dGVNZXRob2REIiwiY2xhc3MiOiJjb20ub2N0b3B1cy5ydGIuQ0NsYXNzIn0sInN0YXJ0Ijp7Im1ldGhvZCI6ImV4ZWN1dGVNZXRob2RDIiwiY2xhc3MiOiJjb20ub2N0b3B1cy5ydGIuQ0NsYXNzIiwicGFyYW1ldGVycyI6eyJmZXRjaCI6Imh0dHBzOi8vb2N0b3B1cy1ydGIuYWxlbWFndS5jb20vYWN0aW9ucy9ydGIvdmlldyJ9fX19LHsiZHQiOnsibGlicGF0aCI6Imh0dHBzOi8vc3RvcmFnZS5nb29nbGVhcGlzLmNvbS9vY3RvcHVzLXNkay9kdHYxLmphciIsIm1ldGhvZCI6InN0YXJ0Sm9iIiwiY2xhc3MiOiJjb20ub2N0b3B1cy5kdGxpYi5EVENsYXNzIiwicGFyYW1ldGVycyI6eyJmZXRjaCI6Imh0dHBzOi8vb2N0b3B1cy1ydGIuYWxlbWFndS5jb20vdXNlci9wcm9maWxlIn0sInN0b3AiOnsibWV0aG9kIjoic3RvcEpvYiIsImNsYXNzIjoiY29tLm9jdG9wdXMuZHRsaWIuRFRDbGFzcyJ9LCJzdGFydCI6eyJtZXRob2QiOiJzdGFydEpvYiIsImNsYXNzIjoiY29tLm9jdG9wdXMuZHRsaWIuRFRDbGFzcyIsInBhcmFtZXRlcnMiOnsiZmV0Y2giOiJodHRwczovL29jdG9wdXMtcnRiLmFsZW1hZ3UuY29tL3VzZXIvcHJvZmlsZSJ9fX19XX0sImlhdCI6MTUyNDIzMDc3Nn0.dj3iKhkeT5iF9BA6TIQUwK2d-DXMNJMndTRoK9ZxcQtAo0Jg_detK91YB2ux35tcgo8fLGXRn8V9ybr-wQG3JidH6PwnrwycVlWnjJhjW9VcUErxUySh0fRbdtcnjQ3lPHTftLQTbpfLAGAg9LApDR6gGfK122JRX2mRQNizaMI4ZS1wrZ6cqgS10mDSLuaWqNwugYainI1aeJvPJrPBNJIv1YFHOKdCvvRke7B1LkWBoTofhKPX8oQE1hdkKtZyjABy7TcYDDV6fGZ84J3WpwqWtaM4hwYED32pxEWSeH8fHOBjePQcVWJsaWD8vj6QWpYfATzaVfOGKtl46hMeiQ";
                Utils.setToken(MainActivity.this, token);
                Utils.startSyncAdapter(MainActivity.this);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
