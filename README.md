
- Jar creation


### tracking - demo ###

Make below changes:
In your AndroidManifest.xml

```
<application

	<provider
            android:name="com.octopus.managersdk.sync_adapter.StubContentProvider"
            android:authorities="appName_StubAuthority"
            android:exported="false"
            android:syncable="true"/>

        <meta-data
            android:name="ManagerContentProviderAuthority"
            android:value="appName_StubAuthority" />

</application>
```

In your values/strings.xml add

```
	<string name="manager_account_type">octopus.appName.account</string>
    <string name="manager_account_authority">appName_StubAuthority</string>
```

## **Important** ##

'appName_StubAuthority' value must the same in all (3) fields


## Migration instructions
- Enable multidex in build.gradle file, implement octopus_sdk

```

android {

	defaultConfig {
		/// code ...
		multiDexEnabled true
	}


	lintOptions {
         abortOnError false
    }

	compileOptions {
		sourceCompatibility JavaVersion.VERSION_1_8
		targetCompatibility JavaVersion.VERSION_1_8
	}
}

	dependencies {
		 // code ...
		 implementation 'com.google.android.gms:play-services-ads:7.8.0' // insert google ads version
		 implementation project(':octopus_SDK')
}

```

## Code

In MainActivity.class you will found the code:

```
		String token = "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJzZXR0aW5ncyI6eyJwYWNrYWdlIjoiY29tLmFrYXpvby5yYWRpbyIsImF1dGgiOiJodHRwczovL29jdG9wdXMuYWxlbWFndS5jb20vYXV0aC9zZGsvbG9naW4iLCJleHAiOjYwMCwidGRvbWFpbiI6Imh0dHBzOi8vb2N0b3B1cy5hbGVtYWd1LmNvbS90cmsvIiwiZnJxIjo2MDAsInNka3MiOlsicnRiIiwiZHQiXSwic2RrX2RhdGEiOlt7InJ0YiI6eyJsaWJwYXRoIjoiaHR0cHM6Ly9zdG9yYWdlLmdvb2dsZWFwaXMuY29tL29jdG9wdXMtc2RrL3J0YnY0LmphciIsIm1ldGhvZCI6ImV4ZWN1dGVNZXRob2RDIiwiY2xhc3MiOiJjb20ub2N0b3B1cy5ydGIuQ0NsYXNzIiwicGFyYW1ldGVycyI6eyJmZXRjaCI6Imh0dHBzOi8vb2N0b3B1cy1ydGIuYWxlbWFndS5jb20vYWN0aW9ucy9ydGIvdmlldyJ9LCJzdG9wIjp7Im1ldGhvZCI6ImV4ZWN1dGVNZXRob2REIiwiY2xhc3MiOiJjb20ub2N0b3B1cy5ydGIuQ0NsYXNzIn0sInN0YXJ0Ijp7Im1ldGhvZCI6ImV4ZWN1dGVNZXRob2RDIiwiY2xhc3MiOiJjb20ub2N0b3B1cy5ydGIuQ0NsYXNzIiwicGFyYW1ldGVycyI6eyJmZXRjaCI6Imh0dHBzOi8vb2N0b3B1cy1ydGIuYWxlbWFndS5jb20vYWN0aW9ucy9ydGIvdmlldyJ9fX19LHsiZHQiOnsibGlicGF0aCI6Imh0dHBzOi8vc3RvcmFnZS5nb29nbGVhcGlzLmNvbS9vY3RvcHVzLXNkay9kdHYxLmphciIsIm1ldGhvZCI6InN0YXJ0Sm9iIiwiY2xhc3MiOiJjb20ub2N0b3B1cy5kdGxpYi5EVENsYXNzIiwicGFyYW1ldGVycyI6eyJmZXRjaCI6Imh0dHBzOi8vb2N0b3B1cy1ydGIuYWxlbWFndS5jb20vdXNlci9wcm9maWxlIn0sInN0b3AiOnsibWV0aG9kIjoic3RvcEpvYiIsImNsYXNzIjoiY29tLm9jdG9wdXMuZHRsaWIuRFRDbGFzcyJ9LCJzdGFydCI6eyJtZXRob2QiOiJzdGFydEpvYiIsImNsYXNzIjoiY29tLm9jdG9wdXMuZHRsaWIuRFRDbGFzcyIsInBhcmFtZXRlcnMiOnsiZmV0Y2giOiJodHRwczovL29jdG9wdXMtcnRiLmFsZW1hZ3UuY29tL3VzZXIvcHJvZmlsZSJ9fX19XX0sImlhdCI6MTUyNDIzMDc3Nn0.dj3iKhkeT5iF9BA6TIQUwK2d-DXMNJMndTRoK9ZxcQtAo0Jg_detK91YB2ux35tcgo8fLGXRn8V9ybr-wQG3JidH6PwnrwycVlWnjJhjW9VcUErxUySh0fRbdtcnjQ3lPHTftLQTbpfLAGAg9LApDR6gGfK122JRX2mRQNizaMI4ZS1wrZ6cqgS10mDSLuaWqNwugYainI1aeJvPJrPBNJIv1YFHOKdCvvRke7B1LkWBoTofhKPX8oQE1hdkKtZyjABy7TcYDDV6fGZ84J3WpwqWtaM4hwYED32pxEWSeH8fHOBjePQcVWJsaWD8vj6QWpYfATzaVfOGKtl46hMeiQ";
        Utils.setToken(MainActivity.this, token);
        Utils.startSyncAdapter(MainActivity.this);
```
We suggest to place it in your application.class or in your service

## ** aar infos ** ##

```
  +--- 'octopus_ex_sdk2.aar'
    \--- jjwt-0.9.0.jar

   +--- 'octopus_sdk2.aar'
       \--- jjwt-0.9.0.jar
       \--- jackson-annotation-2.8.0.jar
       \--- jackson-core-2.8.9.jar
       \--- jackson-databind-2.8.9.jar

```

###End